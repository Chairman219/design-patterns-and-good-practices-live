# README

Purpose of this repository is to share source code of examples I go through with my students at SDA `design-patterns-and-good-practices` module.
To find source code of your group, please checkout to the correct branch, usually named after your group (e.g. `PythonRemoteCZ21`).